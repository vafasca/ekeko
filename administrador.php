﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
    <script src="js/menu.js" type="text/javascript"></script>
    <title>ADMINISTRADOR</title>
</head>

<body>
    <?php
        session_start();
        if(!isset($_SESSION["usuario"]))
        {
            header("location:index.php");
        }
    ?>
    <div class="extra">
        <header>
            <div class="container-fluid">
                <nav class="navbar">
                    <a class="navbar-brand" href="http://www.umss.edu.bo/">
                        <img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">
                    </a>
                    <a class="navbar-brand" href="http://fcyt.umss.edu.bo/">
                        <img src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png" width="130" height="150">
                    </a>
                </nav>
            </div>
        </header>  
        <header>
            <div class="row">
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"> 
                    <a class="navbar-brand" href="logout.php">
                        <!--<button type="button"  class="btn btn-success btn-lg" href="logout.php">CERRAR SESION</button>--> 
                        <input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg">
                    </a>
                </div>
            </div>                         
        </header>
        <?php echo Ver_Datos($_SESSION["usuario"], $_SESSION["rol"]);?>              
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 btn-group-vertical">
                    <ul>
                        <input type="button" name="answer" value="REGISTRAR" onclick="showDiv()" class="btn btn-dark btn-lg espacio1">
                        <input type="button" name="answer" value="NOTIFICACIONES" onclick="showDiv1()" class="btn btn-dark btn-lg espacio1">
                        <input type="button" name="answer" value="EDITAR MIS DATOS" onclick="showDivD()" class="btn btn-dark btn-lg espacio1">
                        <div class="btn-group espacio1">
                            <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">OPCIONES</button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="button">REGLAMENTO</button>
                                <button class="dropdown-item" type="button">CALENDARIO DEFENSAS</button>
                                <button class="dropdown-item" type="button"></button>
                            </div>
                        </div>
                    </ul>
                </div>                          
                <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
                    <form>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="container1">ESTUDIANTE
                                    <input type="radio" checked="checked" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="container1">DOCENTE
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="container1">ADMINISTRATIVOS
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Codigo Sis" class="col-4 col-form-label">Codigo Sis</label>
                            <div class="col-8">
                                <input type="address" class="form-control" id="sname" placeholder="Codigo Sis">
                            </div>
                        </div>                     
                        <div class="form-group row">
                            <label for="Celula de Identidad" class="col-4 col-form-label">Ingresar C.I.</label>
                            <div class="col-8">
                                <input type="address" class="form-control" id="sname" placeholder="Ingresar C.I.">
                            </div>
                        </div>                     
                        <div class="form-group row">
                            <label for="Nombres" class="col-4 col-form-label">Nombres</label>
                            <div class="col-8">
                                <input type="address" class="form-control" id="sname" placeholder="Nombres">
                            </div>
                        </div>                      
                        <div class="form-group row">
                            <label for="Apellido Paterno" class="col-4 col-form-label">Apellido Paterno</label>
                            <div class="col-8">
                                <input type="address" class="form-control" id="sname" placeholder="Apellido Paterno">
                            </div>
                        </div>                       
                        <div class="form-group row">
                            <label for="Apellido Materno" class="col-4 col-form-label">Apellido Materno</label>
                            <div class="col-8">
                                <input type="address" class="form-control" id="sname" placeholder="Apellido Materno">
                            </div>
                        </div>                      
                        <div class="form-group row">
                            <label for="Fecha Nacimiento" class="col-4 col-form-label">Fecha Nacimiento</label>
                            <div class="col-8">
                                <input type="text" id="datepicker" class="form-control" placeholder="Elegir">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <button type="submit" class="btn btn-info">REGISTRAR</button>
                            </div>
                        </div>
                    </form>
                </div>                 
                <div class="wrapper" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">    
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group row">
                                <li class="list-group-item">PERFIL</li>
                                <li class="list-group-item">NOMBRE</li>
                                <li class="list-group-item">APELLIDO PATERNO</li>
                                <li class="list-group-item">APELLIDO MATERNO</li>
                                <li class="list-group-item">FECHA NACIMIENTO</li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item">FECHA LLEGANDO A SU FIN</li>
                                <li class="list-group-item">ARIEL</li>
                                <li class="list-group-item">VIA</li>
                                <li class="list-group-item">MENECES</li>
                                <li class="list-group-item">154236</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div Class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group row">
                                <li class="list-group-item">CI</li>
                                <li class="list-group-item">NOMBRE</li>
                                <li class="list-group-item">APELLIDO PATERNO</li>
                                <li class="list-group-item">APELLIDO MATERNO</li>
                                <li class="list-group-item">FECHA NACIMIENTO</li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item">1249563</li>
                                <li class="list-group-item">ARIEL</li>
                                <li class="list-group-item">VIA</li>
                                <li class="list-group-item">MENECES</li>
                                <li class="list-group-item">154236</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list">
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group row">
                                <li class="list-group-item">CI</li>
                                <li class="list-group-item">NOMBRE</li>
                                <li class="list-group-item">APELLIDO PATERNO</li>
                                <li class="list-group-item">APELLIDO MATERNO</li>
                                <li class="list-group-item">FECHA NACIMIENTO</li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item">1249563</li>
                                <li class="list-group-item">ARIEL</li>
                                <li class="list-group-item">VIA</li>
                                <li class="list-group-item">MENECES</li>
                                <li class="list-group-item">154236</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--</div>--> 
</body>
</html>

