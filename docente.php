<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">  
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
    <script src="js/menu.js" type="text/javascript"></script>
    <title>Docente</title>
</head>

<body>
    <?php
        session_start();
        if(!isset($_SESSION["usuario"]))
        {
            header("location:index.php");
        }
        include "funciones.php";
    ?>
    <div class="extra">
    <header>
            <div class="container-fluid">
                <nav class="navbar">
                    <a class="navbar-brand" href="http://www.umss.edu.bo/">
                        <img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">
                    </a>
                    <a class="navbar-brand" href="http://fcyt.umss.edu.bo/">
                        <img class="esconder" src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png"     width="130" height="150">
                    </a>
                </nav>
            </div>
        </header>
    <header>
        <div class="container-fluid">
            <nav class="navbar">
                <a class="navbar-brand" href="#"></a>
                <a class="navbar-brand" href="logout.php">
                    <!--<button type="button"  class="btn btn-success btn-lg">CERRAR SESION</button>-->
                    <input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg"> 
                </a>
            </nav>
        </div>
    </header> 
    <?php echo Ver_Datos($_SESSION["usuario"], $_SESSION["rol"]);?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 btn-group-vertical">
                <ul>                                
                    <input type="button"  name="answer" value="COMPLETAR REGISTRO" onclick="showDiv()"  class="btn btn-dark btn-lg espacio1">       
                    <input type="button"  name="answer" value="NOTIFICACIONES" onclick="showDiv1()"  class="btn btn-dark btn-lg espacio1">
                    <input type="button"  name="answer" value="EDITAR MIS DATOS" onclick="showDivD()"  class="btn btn-dark btn-lg espacio1"> 
                   <div class="btn-group espacio1">
                                  <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      OPCIONES
                                  </button>
                                  <div class="dropdown-menu dropdown-menu-right">
                                      <button class="dropdown-item" type="button">REGLAMENTO</button>
                                      <button class="dropdown-item" type="button">CONCLUSION</button>
                                      <button class="dropdown-item" type="button">RENUNCIA</button>
                                  </div>                                   
                </ul> 
            </div>
            <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">    
                <form>        
                    <div class="form-group row">
                        <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">AREA</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>REDES</option>
                                <option>INGENIERIA DE SOFTWARE</option>
                                <option>INTELIGENCIA ARTIFICIAL</option>
                                <option>BASE DE DATOS</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exampleFormControlSelect1" class="col-sm-2 col-form-label">SUB-AREA</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>CALIDAD DE SOFTWARE</option>
                                <option>BIG DATA</option>
                                <option>APLICACIONES MOVILES</option>                               
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-info">ENVIAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="wrapper">
                <div id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">DATOS USUARIO
                    <div class="row">
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item">Cras justo odio</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                                <li class="list-group-item">Porta ac consectetur ac</li>
                                <li class="list-group-item">Vestibulum at eros</li>
                            </ul>
                        </div>
                        <div class="col-6">
                            <ul class="list-group">
                                <li class="list-group-item">Cras justo odio</li>
                                <li class="list-group-item">Dapibus ac facilisis in</li>
                                <li class="list-group-item">Morbi leo risus</li>
                                <li class="list-group-item">Porta ac consectetur ac</li>
                                <li class="list-group-item">Vestibulum at eros</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
                        <div class="row">
                            <div class="col-6">
                                <ul class="list-group">
                                    <li class="list-group-item">CI</li>
                                    <li class="list-group-item">NOMBRE</li>
                                    <li class="list-group-item">APELLIDO PATERNO</li>
                                    <li class="list-group-item">APELLIDO MATERNO</li>
                                    <li class="list-group-item">FECHA NACIMIENTO</li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="list-group">
                                    <li class="list-group-item">7896549</li>
                                    <li class="list-group-item">BRUNO ALEJANDRO</li>
                                    <li class="list-group-item">ORTIZ</li>
                                    <li class="list-group-item">VELASQUEZ</li>
                                    <li class="list-group-item">147852</li>
                                </ul>
                            </div>
                        </div>
                    </div>

						 <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list"></div>	
						


                </div>
            </div>    
        </div>      
    </div> 
</body>
</html>

		

