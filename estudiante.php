<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 
    <script src="js/menu.js" type="text/javascript"></script>
    <title>ESTUDIANTE</title>
</head>

<body>
    <?php
        session_start();
        if(!isset($_SESSION["usuario"]))
        {
            header("location:index.php");
        }
        include ("funciones.php");
    ?>
    <div class="extra">
		<header>
            <div class="container-fluid">
                <nav class="navbar">
                    <a class="navbar-brand" href="http://www.umss.edu.bo/">
                        <img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">
                    </a>
                    <a class="navbar-brand" href="http://fcyt.umss.edu.bo/">
                        <img src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png" width="130" height="150">
                    </a>
                </nav>
            </div>
		</header>  		
        <header>
            <div class="row">
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"></div>
                <div class="col"> 
                    <a class="navbar-brand" href="logout.php">
                        <!--<button type="button"  class="btn btn-success btn-lg" href="logout.php">CERRAR SESION</button>--> 
                        <input type="submit" name="enviar" value="CERRAR SESION" class="btn btn-success btn-lg">
                    </a>
                </div>
            </div>                   
        </header> 
        <?php echo Ver_Datos($_SESSION["usuario"], $_SESSION["rol"]);?>
        <div class="col-6">
        <div class="container-fluid">
            <div class="row">
            <div class="col-lg-2 btn-group-vertical">
            <ul>
                <input type="button" name="answer" value="REGISTRAR PERFIL" onclick="showDiv()" class="btn btn-dark btn-lg espacio1">
                <input type="button" name="answer" value="TUTOR" onclick="showDiv1()" class="btn btn-dark btn-lg espacio1">
                <input type="button" name="answer" value="EDITAR MIS DATOS" onclick="showDivD()" class="btn btn-dark btn-lg espacio1">
                <div class="btn-group espacio1">
                    <button type="button" class="btn btn-dark btn-lg dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    OPCIONES
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <button class="dropdown-item" type="button">REGLAMENTO</button>
                        <button class="dropdown-item" type="button">CONCLUSION</button>
                        <button class="dropdown-item" type="button">RENUNCIA</button>
                    </div>
                </div>
            </ul>
        </div>
        <div class="wrapper" id="registroperfilDiv" class="container-fluid" style="display:none;" class="answer_list">
            <form>
                <div class="form-group row">
                    <label for="formGroupExampleInput" class="col-3 col-form-label">TITULO</label>
                    <div class="col-9">
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nombre Completo">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleFormControlSelect1" class="col-3 col-form-label">MODALIDAD</label>
                    <div class="col-9">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>TRABAJO DIRIGIDO</option>
                            <option>PROYECTO DE GRADO</option>
                            <option>BASE DE DATOS</option>
                            <option>ADSCRIPCION</option>
                            <option>PROYECTO DE INVESTIGACION (TESIS) </option>
                            <option>EXCELENCIA</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleFormControlSelect1" class="col-3 col-form-label">AREA</label>
                    <div class="col-9">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>INGENIERIA DE SOFTWARE</option>
                            <option>INTELIGENCIA ARTIFICIAL</option>
                            <option>BASE DE DATOS</option>
                            <option>ROBOTICA</option>
                            <option>CONTABILIDAD</option>
                            <option>TELECOMUNICACIONES</option>
                            <option>REDES</option>
                            <option>REINGENIERIA</option>
                            <option>PROGRMACION MOVIL</option>
                            <option>INFORMATICA</option>
                            <option>SEGURIDAD DE SISTEMAS</option>
                            <option></option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleFormControlSelect1" class="col-3 col-form-label">SUB-AREA</label>
                    <div class="col-9">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>CALIDAD DE SOFTWARE</option>
                            <option>BIG DATA</option>
                            <option>APLICACIONES MOVILES</option>
                            <option>DATA SCIENCE</option>
                            <option>DATA MINING</option>
                            <option>DATA SCIENCE</option>
                            <option>REDES ALEATORIAS</option>
                            <option>TELEFONIA IP</option>
                            <option>DATA SCIENCE</option>
                            <option>SISTEMAS WEB</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="formGroupExampleInput" class="col-3 col-form-label">OBJETIVO GENERAL</label>
                    <div class="col-9">
                        <textarea class="form-control" rows="2" id="comment"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="formGroupExampleInput" class="col-3 col-form-label">OBJETIVOS ESPECIFICOS</label>
                    <div class="col-9">
                        <textarea class="form-control" rows="2" id="comment"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="formGroupExampleInput" class="col-3 col-form-label">DESCRIPCION</label>
                    <div class="col-9">
                        <textarea class="form-control" rows="2" id="comment"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-3">
                        <button type="submit" class="btn btn-info">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>              
        <div class="wrapper" id="welcomeDiv" class="container-fluid" style="display:none;" class="answer_list">    
            <form>        
                <div class="form-group row">
                <label for="exampleFormControlSelect1" class="col-s col-form-label">DOCENTE</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option>Ing. Ayoroa Cardozo Jose Richard</option>
                            <option>Lic. Calancha Navia Boris Marcelo</option>
                            <option>Msc. Costas Jauregui Vladimir</option>
                            <option>Msc. Lic. Rodriguez Bilbao Erika Patricia</option>
                            <option>Lic. Azero Alcocer Pablo Ramon</option>
                            <option>Lic. Garcia Perez Carmen Rosao</option>
                            <option>Msc. Lic. Torrico Bascope Rosemary</option>
                            <option>Lic. Blanco Coca Maria Leticia</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-info">ENVIAR SOLICITUD</button>
                    </div>
                </div>
            </form>
		</div>
        <div Class="wrapper" id="editardatos" class="container-fluid" style="display:none;" class="answer_list">
            <div class="row">
                <div class="col-6">
                    <ul class="list-group row">
                        <li class="list-group-item">CI</li>
                        <li class="list-group-item">NOMBRE</li>
                        <li class="list-group-item">APELLIDO PATERNO</li>
                        <li class="list-group-item">APELLIDO MATERNO</li>
                        <li class="list-group-item">FECHA NACIMIENTO</li>
                    </ul>
                </div>
                <div class="col-6">
                    <ul class="list-group">
                        <li class="list-group-item">1249563</li>
                        <li class="list-group-item">ARIEL</li>
                        <li class="list-group-item">VIA</li>
                        <li class="list-group-item">MENECES</li>
                        <li class="list-group-item">154236</li>
                    </ul>
                </div>
            </div>
        </div>
        <div Class="wrapper" id="area" class="container-fluid" style="display:none;" class="answer_list"></div>
        </div>
        </div>
        </div>
    </div> 
</body>
</html>

