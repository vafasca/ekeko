<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
	<link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen"> 	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" >
	
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script src="js/NewsGoth_400.font.js" type="text/javascript"></script>                  
</head>

<body>
    <div class="extra">
        <!--==============================header=================================-->
        <header>
            <div class="container-fluid">
                <nav class="navbar">
                    <a class="navbar-brand" href="http://www.umss.edu.bo/">
                        <img src="http://casa.fcyt.umss.edu.bo/images/shares/Logo1.png" width="400" height="450">
                    </a>
                    <a class="navbar-brand" href="http://fcyt.umss.edu.bo/">
                        <img class="esconder" src="http://www.umss.edu.bo/wp-content/uploads/2018/01/logo-fcyt.png" width="130" height="150">
                    </a>
                </nav>
            </div>
        </header>
        <!--==============================content================================-->
        <div class="container titulo2">                   
            <div class="row titulo">                         
                <div class="col-8 esconder">
                    <h1>BIENVENIDOS AL SISTEMA DE</h1>                        
                </div> 
                <div class="col-8">
                    <h1>REGISTRO DE PERFILES DE PROYECTOS</h1>                        
                </div> 
                <div class="col-8">
                    <h1>FINALES DE GRADO</h1>                        
                </div>                         
            </div> 
        </div>
        <div class="container-fluid mensaje ">                   
            <div class="row" >
                <div class="col-lg-4 esconder">
                    <h4>
                        El Sistema de Registro de Perfiles de Proyecto de Grado ofrece una 
                        administracion de la información a cabalidad, rapida y correcta.
                        Informacion como el registro de perfil, tutor, asignacion, estado de revisión, 
                        conclusion y defensa del trabajo etc....
                    </h4>                          
                </div>  
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <div>                                                         
                        <form class="col-6" action="login.php" method="post">
                            <div class="form-group">
                                <input type="text" name="login" class="form-control" placeholder="USUARIO">                       
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="CONTRASEÑA">                    
                            </div>
                            <div class="form-group">
                                <a class="navbar-brand" href="login.php">
                                    <input type="submit" name="enviar" value="LOGIN" class="btn btn-dark btn-lg espacio">
                                </a>                   
                            </div>                                   
                        </form>    
                        <div class="container">                   
                            <div class="row">
                                <!-- <div class="col-6">
                                    <div class="col">
                                        <a href="docente.php" class="btn btn-danger" role="button" aria-disabled="true">DOCENTE</a>
                                        <a href="administrador.php" class="btn btn-danger" role="button" aria-disabled="true">ADMINISTRADOR</a>
                                        <a href="directorcarrera.php" class="btn btn-danger" role="button" aria-disabled="true">DIRECTOR CARRERA</a>
                                    </div>	
                                </div> -->
                                <div class="col-6">   							                                                 
                                    <!--<button type="button" class="btn btn-primary">						
                                        <i class="fa fa-user-plus fa-2x"></i>
                                        <a href="registro.php" class="btn btn-primary" role="button" aria-disabled="true">Nuevo Usuario</a>
                                    </button>-->
                                    <a href="registro.php" class="btn btn-primary" role="button" aria-disabled="true">Nuevo Usuario
                                        <button type="button" class="btn btn-primary">						
                                            <i class="fa fa-user-plus fa-2x"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>                                                                               
                </div>
            </div>
        </div>
    </div>
</body>
</html>

