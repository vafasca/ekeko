<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"> 
    <link rel="stylesheet" href="css/zerogrid.css" type="text/css" media="screen">
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/menu.js" type="text/javascript"></script>
</head>

<body>
	<div class="extra1">
        <!--==============================header=================================--> 
        <header>
            <div class="container">
                <a class="navbar-brand" href="index.php">
                    <input type="submit" name="back" value="VOLVER" class="btn btn-dark btn-lg espacio"> 
                </a>                  
                <div class="row">                          
                    <div class="col"></div> 
                    <div class="col"></div> 
                </div>                             
            </div> 
        </header>
        <div class="container titulo3">
            <div class="row">
                <div class="col-3"></div> 
                <div class="col-6">                                                    
                    <input type="button"  name="answer" value="REGISTRO"  class="btn btn-dark btn-lg">                  
                    <form role="form" class="espacio">
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="container1">ESTUDIANTE
                                    <input type="radio" checked="checked" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline radioboton">
                                <label class="container1">DOCENTE
                                    <input type="radio" name="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>						
                        <div class="form-group">
                            <label for="address">Codigo Sis</label>
                            <input type="address" class="form-control" id="sname" placeholder="Codigo Sis">
                        </div>
                        <div class="form-group">
                            <label for="name">Celula Identidad</label>
                            <input type="name" class="form-control" id="fname" placeholder="Ingresar C.I.">
                        </div>
                        <div class="form-group">
                            <label for="name">Nombres</label>
                            <input type="name" class="form-control" id="fname" placeholder="Ingresar Nombres">
                        </div>
                        <div class="form-group">
                            <label for="name">Apellido Paterno</label>
                            <input type="name" class="form-control" id="fname" placeholder="Apellido paterno">
                        </div>
                        <div class="form-group">
                            <label for="name">Apellido Materno</label>
                            <input type="name" class="form-control" id="fname" placeholder="Apellido Materno">
                        </div>
                        <div class="form-group">
                            <label>Fecha Nacimiento</label>
                            <input type="text" id="datepicker" class="form-control" placeholder="Choose">
                        </div>
                        <button type="submit" class="btn btn-default">Registrar</button>
                    </form>
                </div>  
                <div class="col-3"></div>                  
            </div>
        </div>      
    </div>   
</body>
</html>
